﻿using UnityEngine;
using System;
using System.Collections.Generic;
using TMPro;

public class CalculatorLogic : MonoBehaviour
{
    private TextMeshPro textMeshPro;
    private GameObject[] ButtonsArray;
    private int MainValue = 0,
                TmpValue = 0;
    private bool isLeft = true;
    private ButtonScript.SignType tmpsigntype = ButtonScript.SignType.zero;

    private void OnEnable()
    {
        textMeshPro = this.GetComponent<TextMeshPro>();
        textMeshPro.text = "0";
        ButtonsArray = GameObject.FindGameObjectsWithTag(CameraMouseClick.CheckTag);
        foreach (GameObject obj in ButtonsArray)
        {
            if (obj.GetComponent<IButton>() != null) { obj.GetComponent<IButton>().PressedEvent += Calculate; }
        }
    }

    private void OnDisable()
    {
        foreach (GameObject obj in ButtonsArray)
        {
            if (obj.GetComponent<IButton>() != null) { obj.GetComponent<IButton>().PressedEvent -= Calculate; }
        }
    }

    void Calculate(int input)
    {
        switch (input)
        {
            case -4:
                MainValue = 0;
                TmpValue = 0;
                isLeft = true;
                textMeshPro.text = "0";
                tmpsigntype = ButtonScript.SignType.zero;
                break;
            case -3:
                if ((!isLeft) & (tmpsigntype != ButtonScript.SignType.zero) & (TmpValue != 0))
                {
                    switch (tmpsigntype)
                    {
                        case ButtonScript.SignType.plus:
                            MainValue += TmpValue;
                            break;
                        case ButtonScript.SignType.multiple:
                            MainValue *= TmpValue;
                            break;
                    }                    
                }
                textMeshPro.text = Convert.ToString(MainValue);
                TmpValue = 0;
                tmpsigntype = ButtonScript.SignType.zero;
                isLeft = true;
                break;
            case -2:
                if (isLeft)
                {
                    tmpsigntype = ButtonScript.SignType.multiple;
                    isLeft = !isLeft;
                    textMeshPro.text += "*";
                }
                break;
            case -1:
                if (isLeft)
                {
                    tmpsigntype = ButtonScript.SignType.plus;
                    isLeft = !isLeft;
                    textMeshPro.text += "+";
                }
                break;
            case -5:
                if (TmpValue != 0)
                {
                    TmpValue /= 10;
                }
                else if (tmpsigntype != ButtonScript.SignType.zero)
                {
                    tmpsigntype = ButtonScript.SignType.zero;
                }
                else if (MainValue != 0)
                {
                    MainValue /= 10;
                    if (MainValue == 0)
                    {
                        textMeshPro.text = "0";
                        break;
                    }
                }
                else { break; }
                textMeshPro.text = textMeshPro.text.Remove(textMeshPro.text.Length - 1);
                break;
            case 0:
                if ((!isLeft) & (TmpValue != 0)) { TmpValue *= 10; }
                else if ((isLeft) & (MainValue != 0)) { MainValue *= 10; }
                textMeshPro.text += "0";
                break;
            default:
                if (textMeshPro.text == "0") { textMeshPro.text = ""; }
                if (isLeft)
                {
                    MainValue *= 10;
                    MainValue += input;
                }
                else
                {
                    TmpValue *= 10;
                    TmpValue += input;
                }
                textMeshPro.text += Convert.ToString(input);
                break;
        }
    }
}
