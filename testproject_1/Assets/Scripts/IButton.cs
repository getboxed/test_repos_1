﻿using System;

public interface IButton
{
    event Action<int> PressedEvent;
    void Execute();
}
