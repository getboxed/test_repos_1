﻿using System;
using UnityEngine;

public class ButtonScript : MonoBehaviour, IButton
{
    public event Action<int> PressedEvent;
    public SignType signType;

    public enum SignType
    {
        plus = -1,
        multiple = -2,
        equals = -3,
        reset = -4,
        backsp = -5,
        zero = 0,
        one = 1,
        two = 2,
        three = 3,
        four = 4,
        five = 5,
        six = 6,
        seven = 7,
        eight = 8,
        nine = 9
    }

    public void Execute() => PressedEvent?.Invoke((int)signType);    
    
}
