﻿using UnityEngine;

public class CameraMouseClick : MonoBehaviour
{
    public static string CheckTag = "Button";

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, Mathf.Infinity))
            {
                if (hit.transform.CompareTag(CheckTag))
                {
                    hit.transform.GetComponent<IButton>()?.Execute();
                }
            }
        }
    }
}
